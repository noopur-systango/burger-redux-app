import * as actionTypes from "./actions";

const initialState = {
  masala: {
    salad: 0,
    cheese: 0,
    aaloo: 0,
    bacon: 0
  },
  totalPrice: 10
};

const masala_prices = {
    salad: 10,
    cheese: 20,
    aaloo: 30,
    bacon: 40
  };

const reducer = (state = initialState, action) => {
    console.log(state)
  switch (action.type) {
    case actionTypes.ADD_MASALA:
      return {
        ...state,
        masala: {
          ...state.masala,
          [action.masalaName]: state.masala[action.masalaName] + 1
        },
        totalPrice: state.totalPrice + masala_prices[action.masalaName]
      };
    case actionTypes.REMOVE_MASALA:
      return {
        ...state,
        masala: {
          ...state.masala,
          [action.masalaName]: state.masala[action.masalaName] - 1
        },
        totalPrice: state.totalPrice - masala_prices[action.masalaName]
      };
      default:
          return{
              ...state
          }
  }
}

export default reducer;