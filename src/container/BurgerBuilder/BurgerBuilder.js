import React, { Component } from "react";
import Aux from "../../hoc/Aux/Aux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import axios from "../../axios-config";
import Spinner from "../../components/UI/Spinner/Spinner";
import * as actionTypes from '../../store/actions';
import { connect } from 'react-redux';

class BurgerBuilder extends Component {
  state = {
    purchasing: false,
    loading: false
  };

  componentDidMount() {
    axios
      .get("/masala.json")
      .then(response => {
        this.setState({ masala: response.data });
      })
      .catch();
  }

  updatePurchaseState(masala) {
    const sum = Object.keys(masala)
      .map(mKey => {
        return masala[mKey];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);
    return sum > 0;
  }
/* 
  addMasalaHandler = type => {
    const oldCount = this.state.masala[type];
    const updateCount = oldCount + 1;
    const updatedMasala = {
      ...this.state.masala
    };

    updatedMasala[type] = updateCount;
    const priceaddition = masala_prices[type];
    const oldprice = this.state.totalPrice;
    const newprice = oldprice + priceaddition;
    this.setState({ totalPrice: newprice, masala: updatedMasala });
    this.updatePurchaseState(updatedMasala);
  };

  removeMasalaHandler = type => {
    const oldCount = this.state.masala[type];
    if (oldCount <= 0) {
      return;
    }

    const updateCount = oldCount - 1;
    const updatedMasala = {
      ...this.state.masala
    };

    updatedMasala[type] = updateCount;
    const priceaddition = masala_prices[type];
    const oldprice = this.state.totalPrice;
    const newprice = oldprice - priceaddition;
    this.setState({ totalPrice: newprice, masala: updatedMasala });
    this.updatePurchaseState(updatedMasala);
  }; */

  purchaseHandler = () => {
    this.setState({ purchasing: true });
  };

  purchaseHandlerClose = () => {
    this.setState({ purchasing: false });
  };

  purchaseHandlerContinue = () => {
    this.props.history.push('/checkout');
  };


  render() {
    const disabledInfo = {
      ...this.props.ings
    };
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }
    let ordersummary = null;

    const { purchasing } = this.state;

    let burger = <Spinner />;

    if (this.props) {
      burger = (
        <Aux>
          <Burger masala={ this.props.ings }></Burger>
          <BuildControls
            masalaadded={this.props.onMasalaAdded}
            masalaremoved={this.props.onMasalaRemoved}
            disabled={disabledInfo}
            purchasable={ this.updatePurchaseState(this.props.ings)}
            price={ this.props.price } 
            ordered={this.purchaseHandler}
          />
        </Aux>
      );
      ordersummary = (
        <OrderSummary
          masala={this.props.ings}
          price={this.props.price}
          purchasecancel={this.purchaseHandlerClose}
          purchasecontinue={this.purchaseHandlerContinue}
        />
      );
    }

    if (this.state.loading) {
      ordersummary = <Spinner />;
    }
    return (
      <Aux>
        <Modal
          show={ purchasing }
          modalClosed={this.purchaseHandlerClose}
        >
          {ordersummary}
        </Modal>
        {burger}
      </Aux>
    );
  }
}

const mapStateToProps = (state) => ({
      ings: state.masala,  //passes as props to the component
      price: state.totalPrice
   })


const mapDispatchToProps = (dispatch) => ({
      onMasalaAdded: (ingName) => dispatch({type: actionTypes.ADD_MASALA, masalaName: ingName}),
      onMasalaRemoved: (ingName) => dispatch({type: actionTypes.REMOVE_MASALA, masalaName: ingName}),
    })


export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
