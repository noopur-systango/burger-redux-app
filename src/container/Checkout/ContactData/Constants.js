export const DefaultState = {
    orderForm: {
        name: {
          value: "",
          valid: false,
          touched: false,
          validation: {
            required: true
          }
        },
        number: {
          value: "",
          valid: false,
          touched: false,
          validation: {
            required: true,
            minLength: 10,
            maxLength: 10
          }
        },
        street: {
          value: "",
          valid: false,
          touched: false,
          validation: {
            required: true
          }
        },
        email: {
          value: "",
          valid: false,
          touched: false,
          validation: {
            required: true,
            value: ''
          }
        }
      }
  }