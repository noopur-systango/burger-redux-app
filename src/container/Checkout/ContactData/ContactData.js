import React, { Component } from "react";
import Button from "../../../components/UI/Button/Button";
import "./ContactData.css";
import axios from "../../../axios-config";
import Input from "../../../components/UI/Input/Input";
import { checkValidity } from "../ContactData/Validation/Validation";
import { DefaultState } from './Constants';
import { connect } from 'react-redux';

class ContactData extends Component {
  constructor(props) {
    super(props)
    this.state = DefaultState
  }
  orderHandler = event => {
    event.preventDefault();
    const { orderForm } = this.state;
    const order = {
      masala: this.props.ings,
      price: this.props.price,
      orderData: { orderForm } //sending form data to server
    };

    axios
      .post("/order.json", order)
      .then(response => {
        this.props.history.push("/");
      })
      .catch(error => {
        console.log(error);
      });
  };

  inputChangedHandler = (event, key) => {
    const updatedOrderForm = {
      ...this.state.orderForm   //extracts name,number....
    };

    const updatedFormElement = {
      ...updatedOrderForm[key]  //extracts "value" of each key
    };

    updatedFormElement.value = event.target.value;

    //checking validation & setting valid, touched to true
    updatedFormElement.valid = checkValidity(
      updatedFormElement.value,
      updatedFormElement.validation
    );
    updatedFormElement.touched = true;

    updatedOrderForm[key] = updatedFormElement;
    this.setState({ orderForm: updatedOrderForm });
  };

  render() {
    const { orderForm } = this.state;
    let form = (
      <form>
        {Object.keys(orderForm).map(key => (
          <Input
            key={key}
            name={key}
            placeholder={" Your " + key}
            changed={event => {
              this.inputChangedHandler(event, key);
            }}
            invalid={!orderForm[key].valid}
            shouldValidate={orderForm[key].validation}
            touched={!orderForm[key].touched}
          ></Input>
        ))}
        <Button btnType="Success" clicked={this.orderHandler}>
          Order
        </Button>
      </form>
    );

    return (
      <div className={"ContactData"}>
        <h4>Enter your contact data:</h4>
        {form}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  ings: state.masala,  //passes as props to the component
  price: state.totalPrice
})

export default connect(mapStateToProps)(ContactData);
