import React, { Component } from "react";
import Order from "../../components/Order/Order";
import axios from "../../axios-config";


class Orders extends Component {
    state = {
        orders: [],
    }
    componentDidMount() {
        axios.get('/order.json')
        .then(res => {
            console.log(res.data);
            const fetchedOrders = [];
            for(let key in res.data)
            {
                fetchedOrders.push({...res.data[key],id:key});
            }
            this.setState({orders:fetchedOrders});
        })
        .catch(error => {
            console.log(error)
        })
    }

  render() {
    return (
      <div >
       {this.state.orders.map(order => (
           <Order 
           key={order.id}
           masala={order.masala}
           price={order.price}/>
       ))}
      </div>
    );
  }
}

export default Orders;
