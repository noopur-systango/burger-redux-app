import React from 'react';
import './NavItem.css';
import NavItem from './NavItem';

const Navitems = () => 
(
    <ul className={'NavItems'}>
        <NavItem link='/'>Burger Builder</NavItem>
        <NavItem link='/orders'>Orders</NavItem>
    </ul>

);

export default Navitems;