import React from 'react';
import Burger from '../Burger/Burger';
import Button from '../UI/Button/Button';
import './CheckoutSummary.css';


const Checkoutsummary = (props) => {
    return(
        <div className={'CheckoutSummary'}>
        <h1>We hope it tastes better..!!</h1>
        <div style={{height: '300px', width: '100%', margin:'auto'}}>
            <Burger masala={props.masala}/>
        </div>
        <Button btnType="Danger" clicked={props.checkoutCancel}>Cancel</Button>
        <Button btnType="Success" clicked={props.checkoutContinue}>Success</Button>
       
        </div>
    );
}

export default Checkoutsummary;