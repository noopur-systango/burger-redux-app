import React from 'react';
import './Input.css';

const Input = (props) => 
{

    return(
        <div>
            <input 
            className={props.invalid && props.shouldValidate && props.touched ? 'InputElement Invalid' : 'InputElement'} 
            inputtype="input" 
            placeholder={props.placeholder}
            value={props.value}
            onChange={props.changed}
            name={props.name}
            /><br></br>
        </div>
    );
}

export default Input;