import React from 'react';
import  './BurgerMasala.css';
import PropTypes from 'prop-types';

const Burgermasala  = (props) => {

    let masala = null;

    switch(props.type)
    {
        case('bread-bottom'):
        masala = <div className={'BreadBottom'}></div>;
        break;
        
        case('bread-top'):
        masala = (<div className={'BreadTop'}>
            <div className={'Seeds1'}></div>
            <div className={'Seeds2'}></div>
        </div>);
        break;

        case('aaloo'):
        masala = <div className={'Aaloo'}></div>
        break;

        case('cheese'):
        masala = <div className={'Cheese'}></div>
        break;

        case('salad'):
        masala = <div className={'Salad'}></div>
        break;

        case('bacon'):
        masala = <div className={'Bacon'}></div>
        break;

        default:
            masala = null;
    }

    return masala;
};

Burgermasala.prototype ={
type : PropTypes.string.isRequired
}

export default Burgermasala;