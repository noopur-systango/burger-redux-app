import React from 'react';
import Logo from '../Logo/Logo';
import NavItems from '../Navigation/NavItems/NavItems';
import '../SideDrawer/SideDrawer.css';

const Sidedrawer = (props) => 
{
    return(
        <div className={'SideDrawer'}>
            <Logo height="11%"/>
            <nav>
                <NavItems/>
            </nav>
        </div>
    );
};

export default Sidedrawer;