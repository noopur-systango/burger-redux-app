import React from 'react';
import Aux from '../../hoc/Aux/Aux';
//import classes from './Layout.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import './Layout.css';
import Sidedrawer from '../SideDrawer/SideDrawer';

const Layout = (props) => (
    <Aux>
        <Toolbar />
        <Sidedrawer/>
        <main className={'Content'}> 
            {props.children}
        </main>
    </Aux>
);

export default Layout;