import React from "react";
import "./Order.css";

const Order = (props) => {
  const masala = []; /* converting object of masala to array */
  for (let masalaName in props.masala) {
    masala.push({
      masala: masalaName,
      amount: props.masala[masalaName]
    });
  }

  const output = masala.map(key => {
    return (
      <span key={key.name}>
        {key.masala}: ({key.amount}) ,  
      </span>
    );
  });

  return (
    <div className={"Order"}>
      <p>Masala = {output}</p>
      <p>
        Total Price:<strong>Rs.{props.price}</strong>
      </p>
    </div>
  );
};

export default Order;
