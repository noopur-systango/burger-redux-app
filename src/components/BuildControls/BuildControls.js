import React from 'react';
import BuildControl from './BuildControl';
import './BuildControls.css';

const controls = [
    {label:'Salad', type:'salad'},
    {label:'Cheese', type:'cheese'},
    {label:'Bacon', type:'bacon'},
    {label:'Aaloo', type:'aaloo'},
];

const BuildControls = (props) => (
<div className={'BuildControls'}>
<p>Current Price: <strong>Rs.{props.price}</strong></p>
{controls.map((cntrl) => 
(
<BuildControl 
    key={cntrl.label} 
    label={cntrl.label}
    added={() => props.masalaadded(cntrl.type)}
    removed={() => props.masalaremoved(cntrl.type)}
    disabled={props.disabled[cntrl.type]}/>
))}

<button 
    className={'OrderButton'} 
    disabled={!props.purchasable}
    onClick={props.ordered}>ORDER NOW</button>
</div>
)

export default BuildControls;